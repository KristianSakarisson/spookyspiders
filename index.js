var self = require("sdk/self");
var data = self.data;

var pageMod = require("sdk/page-mod");

pageMod.PageMod({
	include: "*",
	contentScriptFile: [data.url("jquery.min.js"), data.url("jquery.highlight-5.js"), data.url("main.js")]
})